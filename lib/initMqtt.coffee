mqtt = require 'mqtt'
config = require '../config/config'

n = 0
all = 1

init = ( cb )->
  global.mqttClient  = mqtt.connect config.mqtt.url, { port: config.mqtt.port, username: config.mqtt.username, password: config.mqtt.password }

  global.mqttClient.on 'connect', ()->
    console.log "connect #{ config.mqtt.url } ok"
    if ++n is all
      cb?()


module.exports =  exports =
  init: init