config = require '../config/config'

register = ( cb )->
  registerInfo = {
    user: process.argv[ 2 ]
    project: process.argv[ 3 ]
    container: process.argv[ 4 ]
    name: process.argv[ 4 ]
    desc: ""
    repository: ""
    version: ""
    startTime: ""
    state: ""
    environment:
      ip: ""
      arch: ""
      os: ""
      node: ""
      cpu: ""
      totalmomory: ""
      freemomory: ""
    services:{

    }
  }


  setTimeout ()->
    global.mqttClient.publish config.registerServerTopic, JSON.stringify( config.registerServiceInfo ), ()->
      console.log 'register server'
  ,5000

  global.mqttClient.publish config.registerTopic, JSON.stringify( registerInfo ), ()->
    cb?()

module.exports =  exports =
  register: register