config = require "../config/config"
child_process = require "child_process"
MB = 1024 * 1024
GB = 1024 * MB

class Container
  constructor: ->
    console.log 'init Container ok'

  gitClone: ( url, service, cb )->
    child_process.exec "git clone -v --progress #{ url } ./.git-clone/#{ service }", { maxBuffer: 1 * GB }, ( error, stdout, stderr )->
      return console.log error if error
      console.log stderr
      cb?()

  gitMicroServiceStart: ( microService, n, cb )->
    child_process.exec "cd ./.git-clone/#{ microService } && pm2 start index.js -i #{ n || 1 } --name #{ microService } && pm2 list", { maxBuffer: 1 * GB }, ( error, stdout, stderr )->
      return console.log error if error
      console.log stderr
      cb?()


  npmInstall: ( npmName, cb )->
    child_process.exec "npm install #{ npmName } --#{ config.registry }", { maxBuffer: 1 * GB }, ( error, stdout, stderr )->
      return console.log error if error
      console.log stderr
      cb?()


  npmMicroServiceStart: ( microService, n, cb )->
    child_process.exec "cd ./node_modules/#{ microService } && pm2 start index.js -i #{ n || 1 } --name #{ microService } && pm2 list", { maxBuffer: 1 * GB }, ( error, stdout, stderr )->
      return console.log error if error
      console.log stderr
      cb?()

  deleteService: ( microService, cb )->
    child_process.exec "pm2 delete #{ microService }", ()->
      cb?()

module.exports =  exports =
  Container: Container