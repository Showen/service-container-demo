mqtt = require './lib/initMqtt'
config = require './config/config'
fs = require 'fs'

Container = new require( './controller/containerCtrl' ).Container
container = new Container()
register = new require( './lib/registerInfo' ).register

if process.argv.length < 5
  return console.log 'start fail. need user,project,container.'

mqtt.init ()->
  console.log 'init mqtt ok'

  register ()->
    console.log 'register info ok'

  global.mqttClient.subscribe config.dispatchTopic
  global.mqttClient.on 'message', ( topic, message )->
    try
      message = JSON.parse( message.toString() )
    catch err
      return console.log err

#    console.log message

#    #git 安装
#    if message.container is process.argv[ 4 ]
#      isExist = fs.existsSync( "./.git-clone/#{ message.service }" )
#
#      if not isExist
#        container.gitClone message.image, message.service, ()->
#          container.gitMicroServiceStart message.service, parseInt( message.instances ) || 1, ()->
#            #TODO:更新控制器的service信息
#            console.log "pm2 #{ message.service } ok"
#      else
#        container.gitMicroServiceStart message.service, parseInt( message.instances ) || 1, ()->
#          console.log "pm2 #{ message.service } ok"

    #npm安装
    if message.container is process.argv[ 4 ]
      isExist = fs.existsSync( "./node_modules/#{ message.name }" )

      console.log message
      if not isExist
        container.npmInstall message.service, ()->
          container.npmMicroServiceStart message.service, parseInt( message.instances ) || 1, ()->
            console.log "pm2 #{ message.service } ok"
      else
        container.npmMicroServiceStart message.service, parseInt( message.instances ) || 1, ()->
          console.log "pm2 #{ message.service } ok"








#  container.gitClone 'https://Showen@bitbucket.org/Showen/micro-service-demo.git', ()->
#    console.log 'load micro-service-demo.git ok'
#    container.gitMicroServiceStart 'micro-service-demo.git', 2,  ()->
#      console.log 'pm2 start micro-service-demo.git ok'
#      service =
#        service: "#{process.argv[ 2 ]}.#{ process.argv[ 3 ] }.micro-service-demo.git }"
#        instanceId: "micro-service-demo.git"
#        pm2:
#          num: 2
#          uptime: "#{ new Date() }"
#          state: "online"
#          id: ""
#          app: ""
#          memory: ""
#          cpu: ""

#      console.log service
#      container.deleteService 'micro-service-demo.git', ()->
#        console.log "delete micro-service-demo.git ok"


