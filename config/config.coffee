envmode = process.env.ENVNODE || 'development'

conf =
  production:
    mqtt:
      url: "mqtt://127.0.0.1"
      port: 1883
      username: 'showen',
      password: '12345'
      will:
        topic: '/register/service-container'
        payload: 'I am lost'
      subscribe: '/micro-service'

    registry: 'https://registry.npm.taobao.org/'

    registerTopic: "register/service-container"
    registerServerTopic: "register/micro-service"
    dispatchTopic: "register/micro-service-instance"

    registerServiceInfo:
      user: 'admin'
      project: 'public'
      service: 'left-pad'
#      name: 'server-1'
      name: 'left-pad'
      image:
        url: 'https://Showen@bitbucket.org/Showen/micro-service-demo.git'

  development:
    mqtt:
      url: "mqtt://127.0.0.1"
      port: 1883
      username: 'showen',
      password: '12345'
      will:
        topic: 'register/service-container'
        payload: 'I am lost'
      subscribe: '/micro-service'

    registry: 'https://registry.npm.taobao.org/'

    registerTopic: "register/service-container"
    registerServerTopic: "register/micro-service"
    dispatchTopic: "register/micro-service-instance"

    registerServiceInfo:
      user: 'admin'
      project: 'public'
      service: 'left-pad'
#      name: 'server-1'
      name: 'left-pad'
      image:
        url: 'https://Showen@bitbucket.org/Showen/micro-service-demo.git'

module.exports = conf[envmode]